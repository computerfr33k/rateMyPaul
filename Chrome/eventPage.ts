declare var chrome;

// Fire when extension is installed or upgraded
chrome.runtime.onInstalled.addListener(function () {
    chrome.storage.local.set({
        update: {
            changelogMsg: "Native Chrome Notifications When Updating Versions",
            show: true
        }
    });

    // Replace all rules
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
        // with a new rule
        chrome.declarativeContent.onPageChanged.addRules([
            {
                // This runs when a page's URL contains compusconnect.depaul.edu
                conditions: [
                    new chrome.declarativeContent.PageStateMatcher({
                        pageUrl: { urlContains: 'https://campusconnect.depaul.edu/psp/'},
                        css: ["#ptifrmtgtframe"]
                    })
                ],

                // Shows the extension page action
                actions: [new chrome.declarativeContent.ShowPageAction()]
            }
        ]);
    });
});

// block RMP images being requested on CampusConnect domain
chrome.webRequest.onBeforeRequest.addListener(
    function (details) {
        return { cancel: true };
    },
    { urls: ["*://campusconnect.depaul.edu/assets/chilis/*"] },
    ["blocking"]
);

/*
 * Generate a random unique id for notification
 */
function getNotificationId() {
    var id = Math.floor(Math.random() * 9007199254740992) + 1;

    return id.toString();
}

/*
 * @param id (string) - Uniquely identify the notification
 * @param opts (json object) - https://developer.chrome.com/apps/notifications#type-NotificationOptions
 * @param cb - Returns the notification id (either supplied or generated) that represents the created notification.
 */
function showChromeNotification(id, opts, cb) {
    if (typeof id == "undefined") {
        id = getNotificationId();
    }

    chrome.notifications.create(id.toString(), opts, cb);
}

/*
 * Extension Message Passing to perform various requests in the background.
 * Set requset.type to determine what kind of message is being sent.
 *
 * Relying on request.url is being deprecated for executing a GET request for a webpage.
 */
chrome.runtime.onMessage.addListener(function (request, sender, callback) {
    console.log(request);
    if (request.type === "notification") {
        showChromeNotification(request.id, request.opts, callback);

    }
    
    if(request.url !== "undefined") 
    {
        var xhr = new XMLHttpRequest();
    
        xhr.onload = function () 
        {
            callback(xhr.responseText);
        };
        xhr.onerror = function () 
        {
            callback();
        };
        
        xhr.open('GET', request.url, true);
        xhr.send();
    };
    
    return true; // prevents the callback from being called too early on return
});
