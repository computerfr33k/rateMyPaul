declare var chrome : any;

class Request
{
    url : string;
    constructor(url : string)
    {
        this.url = url;
    }
    
    private Request = (callback : (data : string) => void, method : string) =>
    {
        chrome.runtime.sendMessage({
            method: method,
            url: this.url
        }, function (data) {
            callback(data);
        });
    }
    
    public Get(callback : (data : string) => void)
    {
        this.Request(callback, "GET");
    }
    
    public Post(callback : (data : string) => void)
    {
        throw "POST NOT SUPPORTED YET";
    }
}

function GetLocalResourceURL(resourceName : string) 
{
    return chrome.extension.getURL(resourceName);
}