//DISCLAIMRER:
//We, Ozer Chagatai and Serguei Fedorov are not responsible for third party modification, repackaging and redistribution of this source code.
//Please be aware that third party distribution of this extention may contain melicious source code which is beyond our control.
//The source code of RateMyPaul, produced by Ozer Chagatai and Serguei Fedorov does not collect and will never collect student and faculty data protected by FERPA.
//This extention only uses the names of DePaul faculty to produce search results. DePaul faculty names are publicly available both through a guest Campus Connect account
//as well as the public facing DePaul Website.
//The complete source code is available on: https://github.com/ochagata/rateMyPaul
//The master branch contains the source code shipped with the extention

declare var chrome : any;

function Cache()
{	
	this.GetRecord = function(recordName, callback)
	{

		var returnRecord = null;
		chrome.storage.local.get(recordName, function(result)
		{
			if ((recordName in result) == false)
			{
				callback(null);
				return;
			}

			var maxCacheTime = 43200000; //12 hours

			if (Date.now() - result[recordName].InsertTime >= maxCacheTime)
			{
				chrome.storage.local.remove(recordName)
				callback(null);
			}
			else
			{
				callback(result[recordName].Data);
			}
		});
	};

	this.InsertRecord = function(recordName, data)
	{
		var CacheObject : any = {};
		CacheObject.InsertTime = Date.now();
		CacheObject.Data = data;

		var SaveObject = {};
		SaveObject[recordName] = CacheObject;

		chrome.storage.local.set(SaveObject);
	};
}