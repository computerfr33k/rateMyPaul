declare var chrome;

function showUpdateNotificationIfNecessary() {

    chrome.storage.local.get("update", function (u) {
        if (u.update.show) {
            var opt = {
                type: "basic",
                title: "RMP Updated!",
                iconUrl: "icon.png",
                message: u.update.changelogMsg
            };

            chrome.runtime.sendMessage({
                type: "notification",
                opts: opt
            }, function () { });
        }
    });
}