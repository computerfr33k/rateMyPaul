#!/usr/bin/env python2

import os
import sys
import shutil
from sys import platform as _platform

print "Currently this script only cleans up the directory before commit"
print "Please make sure your files are NOT in Chrome-Extension, they should be in the normal folder structure."
print "Please do not commit the Chrome-Extension and FireFox-Plugin folders. Those folders WILL BE REMOVED!"

yInput = raw_input("Have you moved your work into (or made your changes in) the Chrome/FireFox/Common/3rdParty folders? Type 'y' for yes:").lower()

CHROME_DIR = "Chrome-Extension"

if yInput == 'y':
	if os.path.exists(CHROME_DIR):
		shutil.rmtree(CHROME_DIR)

	print "You can now do the normal git commands in the new terminal window."
	if _platform == "win32":
		os.system("start \"\" \"%ProgramFiles%\\Git\\bin\\sh.exe\" --login -i")
	elif _platform == "linux" or _platform == "linux2":
		os.system("gnome-terminal")
	elif _platform == "darwin":
		os.system("open -a Terminal \"`pwd`\"")
else:
	print "You chose not to continue."

raw_input("---WINDOW CLOSE STOPPER---")