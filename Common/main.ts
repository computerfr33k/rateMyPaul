//DISCLAIMRER:
//We, Ozer Chagatai and Serguei Fedorov are not responsible for third party modification, repackaging and redistribution of this source code.
//Please be aware that third party distribution of this extention may contain melicious source code which is beyond our control.
//The source code of RateMyPaul, produced by Ozer Chagatai and Serguei Fedorov does not collect and will never collect student and faculty data protected by FERPA.
//This extention only uses the names of DePaul faculty to produce search results. DePaul faculty names are publicly available both through a guest Campus Connect account
//as well as the public facing DePaul Website.
//The complete source code is available on: https://github.com/ochagata/rateMyPaul
//The master branch contains the source code shipped with the extention
//This file relies on:
//LoadingAreaFunctionality.js
//PopupFunctionality.js
//Utilities.js
//Make sure they are imported

///<reference path="Import.ts" />

//declare function CloseUncessaryPopups();
//declare function AppendButtonToProfessorName(professor, profName, ShowRatingClick);
//declare var cache;
//declare function InjectOverallRating(professor, Ratings);
//declare var parser;
//declare function CloseAllNoneFloatingPopups();
//eclare function CreateLoadingArea();
//declare function  CreatePopup(Ratings, profName, URL, index);
//declare function CloseLoadingArea();

$(function(){

    //Entry point
    main();

    function main() {
        setInterval(AugmentPage, 3000);
        //showUpdateNotificationIfNecessary();

    }

    function AugmentPage() 
    {
        if (document.getElementById('ptifrmtgtframe') !== null) 
        {
            var innerDoc = GetInnerDoc();
            CloseUncessaryPopups();

            //This line doesn't work for me. It's always 0 : Serguei
            if (innerDoc.getElementsByClassName("ratingButton").length == 0) 
            {

                $.each(innerDoc.querySelectorAll("span[id ^= 'MTG_INSTR']"), function (index, professor) 
                {

                    var profName = $(professor)[0].innerText;
                    if (profName != 'Staff') 
                    {
                        AppendButtonToProfessorName(professor, profName, ShowRatingClick)

                        //$(professor).append("<input name='" + profName + "' class='ratingButton' type='button' value='SHOW RATING' />");

                        cache.GetRecord(profName, function (returnedData) {
                            if (returnedData != null) 
                            {

                                cache.GetRecord(profName, function (returnedData) 
                                {
                                    if (returnedData != null) {
                                        InjectOverallRating(professor, returnedData.Ratings);
                                    }
                                    else 
                                    {
                                        parser.getProfessorSearch(profName, function (searchResult) 
                                        {
                                            if (searchResult.length != 0) 
                                            {
                                                var pageURL = "http://www.ratemyprofessors.com/" + searchResult[0].URL;
                                                parser.getFullProfessorRatings(pageURL, function (result) {
                                                    InjectOverallRating(professor, result);
                                                    cache.InsertRecord(profName, { Ratings: result, URL: searchResult[0].URL });
                                                });
                                            }
                                            else 
                                            {
                                                InjectOverallRating(professor, null);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
    }
});

function ProcessSearchResults(response, popupDelegate, professorName, index) {
        //Obtain the actual link to the page
        //var searchResult = JSON.parse(response);

    var searchResult = response;

    if (searchResult.length == 0) {
        //We don't have any data from the web service
        popupDelegate([], professorName, "", index);

    } else {
        var pageURL = "http://www.ratemyprofessors.com/" + searchResult[0].URL;

        parser.getFullProfessorRatings(pageURL, function (data) {
            popupDelegate(data, professorName, pageURL, index);
        });
    }
}

function ShowRatingClick() 
{
    var profName = $(this).attr('name');
    
    //Close all the popups to make sure the loading animation isn't playing on top of an existing popup
    CloseAllNoneFloatingPopups();

    //Some names have new lines in them. If that is true, then there either must be more than one
    //professor, or the professor name is repeating (usually the case). We only want the first name
    var profNameSplit = profName.split(/\n/g);
    
    var loader = new LoadingArea();
    loader.CreateLoadingArea(); //Todo: should return reference to loading area

    var alreadyExtractedNames = [];

    $.each(profNameSplit, function (index, item) {
        var parsedItem = item.replace(/,/g, "").trim();

        if (alreadyExtractedNames.indexOf(parsedItem) == -1) {

            cache.GetRecord(profName, function (returnedData) {
                if (returnedData == null) 
                {
                    //Having spaces in between names can create issues for HTTP GET.
                    //format them properly
                    var searchProfString = parsedItem.split(" ").join("%20");

                    parser.getProfessorSearch(searchProfString, function (data) {
                        ProcessSearchResults(data, CreatePopup, parsedItem, index);
                    });
                }
                else 
                {
                    CreatePopup(returnedData.Ratings, profName, "http://www.ratemyprofessors.com/" + returnedData.URL, index);
                }

                alreadyExtractedNames.push(parsedItem);
            });
        }
    });

    loader.CloseLoadingArea();
};
