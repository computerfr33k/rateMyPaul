//DISCLAIMRER:
//We, Ozer Chagatai and Serguei Fedorov are not responsible for third party modification, repackaging and redistribution of this source code.
//Please be aware that third party distribution of this extention may contain melicious source code which is beyond our control.

//The source code of RateMyPaul, produced by Ozer Chagatai and Serguei Fedorov does not collect and will never collect student and faculty data protected by FERPA.
//This extention only uses the names of DePaul faculty to produce search results. DePaul faculty names are publicly available both through a guest Campus Connect account
//as well as the public facing DePaul Website.

///<reference path="../../Import.ts" />
///<reference path="../Models/ProfessorName.ts" />
///<reference path="../Models/ProfessorGrade.ts" />

function GetElementWithoutChildren(element)
{
    return $(element).clone().children().remove().end();
}

class RMPParser
{
    
    private static SearchProcessor = class
    {
        GetProfessorNames = (parsedHTML) =>
        {
            var returnData : Professor[] = [];

            $(parsedHTML).find(".listing").each(function(index, item)
            {
                returnData.push({ Name    : $(item).find(".main").text(),
                                  College : $(item).find(".sub").text(),
                                  URL     : $(item).find("a").attr("href")});
            });

            return returnData;
        }
    }

    
    private static CommentsProcessor = class
    {
        GetProfessorReviews = (parsedHTML : string) =>
        {
            var returnData = [];

            //Find the comments td and then look for the closest TR.
            //individual reviews don't have a defined class.
            $(parsedHTML).find(".comments").each(function(index, item)
                                                    {
                returnData.push({ "Class" : GetElementWithoutChildren($(this).siblings(".class").find(".name .response")).text(),
                                    "Comment" : $(this).find("p").text().trim()
                                });
            });

            return returnData;
        }
    }
    
    private static ReviewPageProcessor = class
    {
        public GetProfessorGrades = (parsedHTML : string) =>
        {
            var returnData : ProfessorGrade[] = [];

            $(parsedHTML).find(".breakdown-header").each(function(index : Number, item : Element) {
                                
                returnData.push({
                   Label :  GetElementWithoutChildren(item).text().trim(),
                   Grade : parseFloat($(item).find(".grade").text().trim())
                });
                
            });

            return returnData;
        }

        public GetProfessorRatings = (parsedHTML) =>
        {
            var returnData : ProfessorRating[] = [];

            //The page returns 6 ratings. Only the first 3 appear to be valid
            $(parsedHTML).find(".rating-slider").slice(0, 3).each(function(index, item)
                                                                    {
                returnData.push({ Label  : $(item).find(".label").text(),
                                  Rating : parseFloat($(item).find(".rating").text())
                                });

            });

            return returnData;
        }
    }
    
    searchURL : string = "http://www.ratemyprofessors.com/search.jsp?queryoption=HEADER&queryBy=teacherName&schoolName=DePaul+University&schoolID=1389&query=";

    //Removes any HTML that is not necessary
    private StripDownHTML = (parsedHTML) =>
    {
        return $(parsedHTML).find("img").remove().end();
    }

    public getFullProfessorRatings(URL : string, responseCallback)
    {
        let parent = this;
        const request = new Request(URL);
        request.Get(function(data : string) {
            if(data == null) {
                throw new Error("Data received from RMP is null.");
            }

            const reducedHTML = parent.StripDownHTML(data);

            var reviewProcessor = new RMPParser.ReviewPageProcessor();
            var ratings = reviewProcessor.GetProfessorRatings(data);
            var grades = reviewProcessor.GetProfessorGrades(data);

            var commentsProcessor = new RMPParser.CommentsProcessor();
            var comments = commentsProcessor.GetProfessorReviews(data);

            var res : any = {};
            res.Grades = grades;
            res.Ratings = ratings;
            res.Comments = comments;

            responseCallback(res);
        });

    }

    public getProfessorSearch(name, responseCallback : (result : Professor[]) => void)
    {
        var searchURLFinal = this.searchURL + name.split(" ").join("%20");

        const request = new Request(searchURLFinal);
        request.Get(function(data) {
            if(data == null) {
                throw new Error("Data recieved from RMP is null");
            }

            //data = StripDownHTML(data);
            var processor = new RMPParser.SearchProcessor();
            var res : Professor[] = processor.GetProfessorNames(data);

            responseCallback(res);

            return true;
        });
    }
}
