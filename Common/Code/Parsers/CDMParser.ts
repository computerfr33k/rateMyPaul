///<reference path="../../Import.ts" />

class CDMParser
{
    private MakeRequest = (url, callback) =>
    {
        const request = new Request(url);
		request.Get(callback);
    }
    
	GetCourseInformation = (courseAbr : string, courseNumber : string, callback) =>
	{
		var URL = "http://odata.cdm.depaul.edu/Cdm.svc/Courses?$filter=SubjectId%20eq%20%27"+ courseAbr +"%27%20and%20" + courseNumber + "%20eq%20%27205%27%20&$format=json"
        this.MakeRequest(URL, callback)
	}

	GetFacultyInformation = (firstName : string, lastName : string, callback) =>
	{
		var URL = "http://odata.cdm.depaul.edu/Cdm.svc/Faculties?$filter=FirstName%20eq%20%27Jean%27%20and%20LastName%20eq%20%27Hop%27&$format=json"
        this.MakeRequest(URL, callback)
	}

	GetFacultyReviews = (firstName : string, lastName : string, callback) =>
	{
		this.GetFacultyInformation(firstName, lastName, function(searchResult)
		{
            this.MakeRequest(searchResult.Evaluations, callback)
			//CreateGetRequest(searchResult.Evaluations, callback);
		});
	}
}