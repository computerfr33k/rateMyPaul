//DISCLAIMRER:
//We, Ozer Chagatai and Serguei Fedorov are not responsible for third party modification, repackaging and redistribution of this source code.
//Please be aware that third party distribution of this extention may contain melicious source code which is beyond our control.

//The source code of RateMyPaul, produced by Ozer Chagatai and Serguei Fedorov does not collect and will never collect student and faculty data protected by FERPA.
//This extention only uses the names of DePaul faculty to produce search results. DePaul faculty names are publicly available both through a guest Campus Connect account
//as well as the public facing DePaul Website.

//The complete source code is available on: https://github.com/ochagata/rateMyPaul
//The master branch contains the source code shipped with the extention

///<reference path="../../3rdParty/jquery.d.ts" />

declare interface HTMLElement {
    contentDocument : any;
    contentWindow : any;
}

function ProfessorNameStillOnPage(professorName : string, documentToSearch : Document)
{
    var professorFound = false;

    professorName = professorName.trim();

    $.each(documentToSearch.querySelectorAll("span[id ^= 'MTG_INSTR']"), function(index, professor) {
        var compareToProfessor = $(professor)[0].innerText;
        compareToProfessor = compareToProfessor.trim();

        if (compareToProfessor.trim().indexOf(professorName) > -1) {
            professorFound = true;
            return false; //Returns out of the the each loop
        }
    });

    return professorFound;
}


function GetInnerDoc()
{
    var iframe = document.getElementById('ptifrmtgtframe');
    var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
    return innerDoc;
}
