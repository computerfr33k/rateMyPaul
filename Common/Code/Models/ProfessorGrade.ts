interface Rating
{
     Label : string,   
}

interface ProfessorGrade extends Rating
{
    Grade : number
}

interface ProfessorRating extends Rating
{
    Rating : number
}