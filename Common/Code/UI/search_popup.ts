///<reference path="../../../3rdParty/jquery.d.ts" />
///<reference path="../../../Chrome/Get_Driver.ts" />
///<reference path="../Parsers/RMPParser.ts" />

declare interface HTMLElement
{
    value : any;
}


function searchButtonClicked() {
    var searchProfString = document.getElementById("SearchInput").value.split(" ").join("%20");
    if(searchProfString.length == 0) {
        $('#ErrorLog').text("Please enter a professor's name");
        $('#ErrorLog').show();
        return;
    } else {
        $('#ErrorLog').hide();
    }

    var searchURL = "http://www.ratemyprofessors.com/search.jsp?queryoption=HEADER&queryBy=teacherName&schoolName=DePaul+University&schoolID=1389&query=";
    var searchURLFinal = searchURL + searchProfString;

    var parser = new RMPParser();

    parser.getProfessorSearch(searchProfString, function(data) {

        if (data.length == 0)
        {
            $('#ErrorLog').text("Search did not produce any results.");
            $('#ErrorLog').show();
            return;
        }

        var url = "http://www.ratemyprofessors.com" + data[0].URL;
        var Name = data[0].Name;

        parser.getFullProfessorRatings(url, function(res : any) {

            $(res.Grades).each(function(index, item : any) {
                switch(item.Label) {
                    case "Overall Quality":
                        $('#OverallQuality').text(item.Rating);
                        break;
                    case "Average Grade":
                        $('#AverageGrade').text(item.Rating);
                        break;
                }
            });

            $(res.Ratings).each(function(index, item : any) {
                switch(item.Label) {
                    case "Helpfulness":
                        $('#Helpfulness').text(item.Rating);
                        break;
                    case "Clarity":
                        $('#Clarity').text(item.Rating);
                        break;
                    case "Easiness":
                        $('#Easiness').text(item.Rating);
                        break;
                }
            });

            $.each(res.Comments, function(index, item) {
                
                const request = new Request(GetLocalResourceURL("ReviewSection.html"));
                request.Get(function(ReviewHTML) {
                    ReviewHTML = ReviewHTML.replace("__COURSE_NUMBER__", item.Class);
                    ReviewHTML = ReviewHTML.replace("__COURSE_COMMENT__", item.Comment);

                    $(ReviewHTML).appendTo($("#ratingsTab", window.parent.document));
                });
            });

            $('#ProfessorName').text(Name).attr('target', '_blank').attr('href', "http://www.ratemyprofessors.com" + data[0].URL);

            $("#ResultArea").show();
        });
    });
}


document.addEventListener('DOMContentLoaded', function() {
    $("#SearchSubmit").on("click", function()
    {   
        searchButtonClicked();
    });

    // hide search results section on page load.
    $("#ResultArea").hide();
});
