//DISCLAIMRER: 
//We, Ozer Chagatai and Serguei Fedorov are not responsible for third party modification, repackaging and redistribution of this source code. 
//Please be aware that third party distribution of this extention may contain melicious source code which is beyond our control.

//The source code of RateMyPaul, produced by Ozer Chagatai and Serguei Fedorov does not collect and will never collect student and faculty data protected by FERPA.
//This extention only uses the names of DePaul faculty to produce search results. DePaul faculty names are publicly available both through a guest Campus Connect account
//as well as the public facing DePaul Website.

//The complete source code is available on: https://github.com/ochagata/rateMyPaul
//The master branch contains the source code shipped with the extention

///<reference path="../../../3rdParty/jquery.d.ts" />

//declare var popupClosureDelegate : any;
//declare function CreateGetRequest(url, func);
//declare function ProcessSearchResults(a : any, b : any, c : any);

(function()
{

	function EVENT_Search_Button_Clicked()
	{
		var searchValue = $("#SearchInput").val();

		if (searchValue.length != 0)
		{
			var searchUrl = 'http://www.sergueifedorov.com/rmpapi/search/' + $("#SearchInput").val();

	        ClosePopup(null);
	        CreateLoadingArea();
    	}
	}

	function ApplyEventHandlers()
	{
		$("#SearchSubmit").click(EVENT_Search_Button_Clicked);
	}

/*
	//Stupdily hacky way because the iFrame caseus two different search bars to render
	if ($("#ptifrmtgtframe").length != 0)
	{
        /*
		CreateGetRequest(GetLocalResourceURL('search.html'), function(html)
		{
			$("body").append(html);
			ApplyEventHandlers();
		});
	}*/
})();