//DISCLAIMRER:
//We, Ozer Chagatai and Serguei Fedorov are not responsible for third party modification, repackaging and redistribution of this source code.
//Please be aware that third party distribution of this extention may contain melicious source code which is beyond our control.
//The source code of RateMyPaul, produced by Ozer Chagatai and Serguei Fedorov does not collect and will never collect student and faculty data protected by FERPA.
//This extention only uses the names of DePaul faculty to produce search results. DePaul faculty names are publicly available both through a guest Campus Connect account
//as well as the public facing DePaul Website.
//The complete source code is available on: https://github.com/ochagata/rateMyPaul
//The master branch contains the source code shipped with the extention

///<reference path="../../Import.ts" />

function AppendButtonToProfessorName(professorEelement, professorName, clickHandler)
{
	var element = $(professorEelement).append(function()
		{
			return $("<input name='" + professorName + "' class='ratingButton' type='button' value='SHOW RATING' />").click(clickHandler);
		});
}

function InjectOverallRating(professor, parsedData)
{
    var table = $(professor).closest("tbody"); 
    if (parsedData == null) 
    {
        table.find("tr:eq(0)").first().append("<th class='PSLEVEL1GRIDCOLUMNHDR'>Overall Rating</th>");
        table.find("tr:eq(1)").append("<td class='PSLEVEL3GRIDROW'> <div class='SSSIMAGECENTER' style='font-weight: bold;'>N/A</div></td>");
    } 
    else 
    {

        table.find("tr:eq(0)").first().append("<th class='PSLEVEL1GRIDCOLUMNHDR'>Overall Rating</th>");
        table.find("tr:eq(1)").append("<td class='PSLEVEL3GRIDROW'> <div class='SSSIMAGECENTER' style='font-weight: bold;'>" + parsedData.Grades[0].Rating + "</div></td>");
            
    }
}