/* global popupClosureDelegate */

//DISCLAIMRER: 
//We, Ozer Chagatai and Serguei Fedorov are not responsible for third party modification, repackaging and redistribution of this source code. 
//Please be aware that third party distribution of this extention may contain melicious source code which is beyond our control.

//The source code of RateMyPaul, produced by Ozer Chagatai and Serguei Fedorov does not collect and will never collect student and faculty data protected by FERPA.
//This extention only uses the names of DePaul faculty to produce search results. DePaul faculty names are publicly available both through a guest Campus Connect account
//as well as the public facing DePaul Website.

//The complete source code is available on: https://github.com/ochagata/rateMyPaul
//The master branch contains the source code shipped with the extention

///<reference path="../../../3rdParty/jquery.d.ts" />

//Controls how the popup get's closed. This is useful if
//the search opens the popup and nullifies the close routine
var popupClosureDelegate = null;

///<reference path="../../Import.ts" />
///<reference path="Code/UI/PopupFunctionality.ts" />

//declare function PopupIsOpen();
//declare function ClosePopup();

declare var iframe;


(function()
{
	setInterval(function()
    {
        var iframe = document.getElementById('ptifrmtgtframe');
        
        if (iframe != null)
        {
            var innerDoc = iframe.contentDocument || iframe.contentWindow.document;

            if (PopupIsOpen() && popupClosureDelegate != null && popupClosureDelegate($(".ProfessorName")[0].innerText, innerDoc) == false)
            {
                 ClosePopup(null);
            }
        }

    }, 2000);
})