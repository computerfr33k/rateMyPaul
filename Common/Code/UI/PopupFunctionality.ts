//DISCLAIMRER: 
//We, Ozer Chagatai and Serguei Fedorov are not responsible for third party modification, repackaging and redistribution of this source code. 
//Please be aware that third party distribution of this extention may contain melicious source code which is beyond our control.

//The source code of RateMyPaul, produced by Ozer Chagatai and Serguei Fedorov does not collect and will never collect student and faculty data protected by FERPA.
//This extention only uses the names of DePaul faculty to produce search results. DePaul faculty names are publicly available both through a guest Campus Connect account
//as well as the public facing DePaul Website.

//The complete source code is available on: https://github.com/ochagata/rateMyPaul
//The master branch contains the source code shipped with the extention

//Forward binds events. These will apply to any element that will get
//added to the DOM which has the provided ID


//declare function GetLocalResourceURL(resource);
//declare function ProfessorNameStillOnPage(name, innerDocument);
//declare function GetInnerDoc();
//declare function CloseLoadingArea();
//declare function CreateGetRequest(url, func);

///<reference path="../../Import.ts" />

declare interface JQuery
{
    draggable : any;
    accordion : any;
}

$("body", window.parent.document).on("click", "#CloseButton", function () {
    $(this).closest(".RMPDisplayArea").remove();
});

$("body").on("click", "#PinButton", function()
{
    if ($(this).closest(".RMPDisplayArea").is(".Pinned"))
    {
        $(this).closest(".RMPDisplayArea").removeClass("Pinned")
        $(this).attr("src", GetLocalResourceURL('window-unlocked.png'));
    }
    else
    {
        $(this).closest(".RMPDisplayArea").addClass("Pinned");
        $(this).attr("src", GetLocalResourceURL('window-locked.png'));
    }
});

$("body", window.parent.document).on("drag", ".RMPDisplayArea", function()
{
    $(this).addClass("Floating");
});

$("body", window.parent.document).on("click", ".RMPDisplayArea", function()
{
    $(".RMPDisplayArea").each(function(index, item)
    {
        $(this).css("zIndex", 1);
    });

    $(this).css("zIndex", 999);
});

function ClosePopup(popup) {
    $(popup).remove();
}

function CloseAllNoneFloatingPopups()
{
    $(".RMPDisplayArea", window.parent.document).each(function(index, item)
    {
        if ($(item).is(".Floating") == false)
        {
            ClosePopup(item);
        }
    });
}

function CloseUncessaryPopups()
{
    var innerDoc = GetInnerDoc();

    $(".RMPDisplayArea").each(function(index, item)
    {
        if ($(item).is(".Pinned") == false)
        {
            if (ProfessorNameStillOnPage($(this).find(".ProfessorNameLink").text(), innerDoc) == false)
            {
                $(this).remove();
            }
        }
    });
}

function CloseAllUnpinnedPopups()
{
    $(".RMPDisplayArea", window.parent.document).each(function(index, item)
    {
        if ($(item).is(".ui-draggable-disabled") == false)
        {
            ClosePopup(item);
        }
    });
}

function PopupIsOpen()
{
    return $(".RMPDisplayArea").is(":visible");
}

function CreatePopup(result, profName, pageUrl, index) {
        
        function AppendPopupToBody(html)
        {
            var newPopup = $(html).appendTo($("body", window.parent.document));
            $(newPopup).css("right", (index * 330) + 50 )

            return newPopup;
        }

	    //Sudo-Constant Vars (no such thing as constants in JavaScript)
	    const PROFESSOR_NAME_FIELD = /__PROFESSOR_NAME__/g;
	    const OVERALL_QUALITY_FIELD = /__OVERALL_QUALITY__/g;
	    const AVERAGE_GRADE_FIELD = /__AVERAGE_GRADE__/g;
	    const HELPFULNESS_FIELD = /__HELPFULNESS__/g;
	    const CLARITY_FIELD = /__CLARITY__/g;
	    const EASINESS_FIELD = /__EASINESS__/;
	    const LINK_TO_PROFESSOR_PAGE = /__LINK_TO_PROFESSOR_PAGE__/g;
	    const CLOSE_BUTTON = /__CLOSE_BUTTON__/g;
	    const LINK_ICON = /__LINK_ICON__/g;
        const PIN_ICON = /__PIN_ICON__/g;

        //If the user keeps clicking, it will open multiple display areas on top of each other
        var loadingArea = new LoadingArea();
        loadingArea.CloseLoadingArea();

        const request = new Request(GetLocalResourceURL("popup.html"));
        //Load the template HTML file
        request.Get(function(html) {

            html = html.replace(CLOSE_BUTTON, GetLocalResourceURL('close.png'));
            html = html.replace(LINK_ICON, GetLocalResourceURL('link.png'));
            html = html.replace(PIN_ICON, GetLocalResourceURL('window-unlocked.png'));

            if (result.Grades != undefined && result.Ratings != undefined && result.Ratings.length > 0 ) {

                html = html.replace(PROFESSOR_NAME_FIELD, profName);
                html = html.replace(OVERALL_QUALITY_FIELD, result.Grades[0].Rating);
                html = html.replace(AVERAGE_GRADE_FIELD, result.Grades[1].Rating);

                html = html.replace(HELPFULNESS_FIELD, result.Ratings[0].Rating);
                html = html.replace(CLARITY_FIELD, result.Ratings[1].Rating);
                html = html.replace(EASINESS_FIELD, result.Ratings[2].Rating);

                html = html.replace(LINK_TO_PROFESSOR_PAGE, pageUrl);

                const popup = AppendPopupToBody(html);

                const commentSectionRequest = new Request(GetLocalResourceURL("ReviewSection.html"));
                $.each(result.Comments, function(index, item)
                {
                    commentSectionRequest.Get(function(ReviewHTML)
                    {
                        ReviewHTML = ReviewHTML.replace("__COURSE_NUMBER__", item.Class);
                        ReviewHTML = ReviewHTML.replace("__COURSE_COMMENT__", item.Comment);

                        $(ReviewHTML).appendTo($("#ratingsTab", popup));
                    });
                });


            } else {

                const naString = "N/A";

                html = html.replace(PROFESSOR_NAME_FIELD, profName);
                html = html.replace(OVERALL_QUALITY_FIELD, naString);
                html = html.replace(AVERAGE_GRADE_FIELD, naString);

                html = html.replace(HELPFULNESS_FIELD, naString);
                html = html.replace(CLARITY_FIELD, naString);
                html = html.replace(EASINESS_FIELD, naString);

                html = html.replace(LINK_TO_PROFESSOR_PAGE, "http://www.ratemyprofessors.com/search.jsp?queryoption=HEADER&queryBy=teacherName&query=" + profName);

                AppendPopupToBody(html);
            }


            $(".RMPDisplayArea", window.parent.document).draggable();
            $(".RMPMain", window.parent.document).accordion({ heightStyle: "content", collapsible: true });
        });
    }
