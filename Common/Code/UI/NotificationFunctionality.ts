/*
 * HTML 5 Notification
 */

///<reference path="../../Import.ts" />

declare var Notification;
declare var chrome;
declare var toastr;

class NotificationSystem
{
    showNotification = (title : string, message : string) =>
    {
        Notification.requestPermission(function (perm) {
            if (perm === 'denied') {
                /* Fallback to using alert or maybe some other javascript toast framework */
                alert(message);
                return;
            } else if (perm === 'default') {
                return;
            }

            var notification = new Notification(title, {
                dir: "auto",
                lang: "",
                body: message,
                tag: message,
                icon: chrome.runtime.getURL("icon.png")
            });
        });
    }

    showUpdateNotificationIfNecessary = () =>
    {
        var parent = this;
        chrome.storage.local.get("update", function (data) {
            if (data.update.show) {
                parent.showNotification("RateMyPaul Updated!", data.update.changelogMsg);

                chrome.storage.local.clear(function () {
                    console.log("storage cleared!");
                });
            }
        });
    }

    ShowToast = (message : string, title : string, options : string, OnHide) =>
    {
        toastr.options = options;
        if (typeof OnHide !== 'undefined') {
            toastr.options.onHidden = OnHide;
        }
        toastr.info(message, title);
    }

    ShowUpdateToastIfNecessary = () =>
    {
        var toastOptions = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": 0,
            "extendedTimeOut": 0,
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "slideDown",
            "hideMethod": "slideUp",
            "tapToDismiss": false
        }
        
        var toastOnHide = function () {
            chrome.storage.local.set({
                update: {
                    show: false,
                    changelogMsg: ""
                }
            });
        }

        chrome.storage.local.get("update", function (data) {
            if (data.update.show) {
                this.ShowToast(data.update.changelogMsg, "Extension Updated!", toastOptions, toastOnHide);
            }
        });
    }
}
