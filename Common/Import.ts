//Imports all the necessary files

//Libraries
/// <reference path="../3rdParty/jquery.d.ts" />

//Parsers
/// <reference path="Code/Parsers/RMPParser.ts" />
///<reference path="Code/Parsers/CDMParser.ts" />

//UI
///<reference path="Code/UI/LoadingAreaFunctionality.ts" />
///<reference path="Code/UI/NotificationFunctionality.ts" />
///<reference path="Code/UI/PageAugmentation.ts" />
///<reference path="Code/UI/PopupFunctionality.ts" />
///<reference path="Code/UI/PopupManager.ts" />
///<reference path="Code/UI/search_popup.ts" />
///<reference path="Code/UI/SearchElementFunctionality.ts" />

///<reference path="Code/Utilities.ts" />
///<reference path="Code/GlobalInstantiations.ts" />

//Must be depricated TODO
//declare function ClosePopup(item);
declare function CreateLoadingArea();

declare var iframe : any;
