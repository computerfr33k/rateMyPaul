#!/usr/bin/env python2

import json
import os

if os.path.isfile("Common_Manifest.json"):
    with open("Common_Manifest.json") as common_manifest_file:
        data = json.load(common_manifest_file)

		outputJSON = {}
		outputJSON["background"] = data["CHROME_BACKGROUND"]
		outputJSON["manifest_version"] = 2
		outputJSON["name"] = data["APP_NAME"]
		outputJSON["version"] = data["APP_VERSION"]
		outputJSON["author"] = []
		outputJSON["description"] = data["DESCRIPTION"]
		outputJSON["icons"] = data["APP_ICONS"]
		outputJSON["page_action"] = {}
		outputJSON["permissions"] = []
		outputJSON["web_accessible_resources"] = []
		outputJSON["content_scripts"] = []

		for author in data["AUTHORS"]:
			outputJSON["author"].append(author)
			
		for action in data["CHROME_PAGE_ACTIONS"]:
			outputJSON["page_action"][action] = data["CHROME_PAGE_ACTIONS"][action]
			
		for permission in data["CHROME_PERMISSIONS"]:
			outputJSON["permissions"].append(permission)
			
		tmp = []
		for resource_type in data["RESOURCES"]:
			for resource in data["RESOURCES"][resource_type]:
				tmp.append(resource)
			
		outputJSON["web_accessible_resources"] = tmp
		
		#outputJSON["content_scripts"].append(data["CHROME_CONTENT_SCRIPTS"])
		
		# Build content scripts section
		SCRIPTS = {}
		SCRIPTS["css"] = []
		SCRIPTS["js"] = []
		for file in os.listdir("Chrome-Extension"):
			if file.endswith(".css"):
				SCRIPTS["css"].append(file)
			elif file.endswith(".js"):
				SCRIPTS["js"].append(file)
				
		outputJSON["content_scripts"].append({})
		outputJSON["content_scripts"][0] = SCRIPTS
		outputJSON["content_scripts"][0]["matches"] = data["CHROME_CONTENT_SCRIPTS"]["matches"]
		outputJSON["content_scripts"][0]["all_frames"] = data["CHROME_CONTENT_SCRIPTS"]["all_frames"]
		outputJSON["content_scripts"][0]["run_at"] = data["CHROME_CONTENT_SCRIPTS"]["run_at"]

		print json.dumps(outputJSON, sort_keys=True)

else:
	print "Could not locate the 'Common_Manifest.js' file"
        outputJSON = {}
        outputJSON["manifest_version"] = 2
        outputJSON["name"] = data["APP_NAME"]
        outputJSON["version"] = data["APP_VERSION"]

        for author in data["AUTHORS"]:
            outputJSON["AUTHOR"] = author

        print outputJSON

else:
    print "Could not locate the 'Common_Manifest.js' file"
