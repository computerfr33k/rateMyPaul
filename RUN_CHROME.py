#!/usr/bin/env python2

import os
import subprocess
import sys
import tempfile
import argparse
import shutil
from sys import platform as _platform

cwd = sys.path[0]
CHROME_EXTENSION_DIR = cwd + "/Chrome-Extension"
TEMP_DIR = tempfile.mkdtemp()
TEMP_CHROME_PROFILE = TEMP_DIR + "/Chrome-Temp-Profile"

ccUrl = "https://campusconnect.depaul.edu/psp/CSPRD90/EMPLOYEE/HRMS/c/COMMUNITY_ACCESS.CLASS_SEARCH.GBL?ver=2&PORTALPARAM_PTCNAV=HC_CLASS_SEARCH_GBL_2&EOPP.SCNode=HRMS&EOPP.SCPortal=EMPLOYEE&EOPP.SCName=ADMN_GUEST_LINKS&EOPP.SCLabel=&EOPP.SCPTcname=PT_PTPP_SCFNAV_BASEPAGE_SCR&FolderPath=PORTAL_ROOT_OBJECT.HCCC_SS_CATALOG.HC_CLASS_SEARCH_GBL_2&IsFolder=false"

if not os.path.exists(TEMP_DIR):
	os.makedirs(TEMP_DIR)

FULL_CHROME_ARGS = "-user-data-dir=" + TEMP_CHROME_PROFILE + " --load-extension=" + CHROME_EXTENSION_DIR + " \"" +  ccUrl + "\"";

if _platform == "win32":
	os.system("py BUILD_CHROME_PROJECT.py")
	os.system("start chrome " + "-user-data-dir=" + TEMP_CHROME_PROFILE + " --load-extension=" + CHROME_EXTENSION_DIR + " \"" +  ccUrl + "\"")
elif _platform == "darwin":
	os.system("python BUILD_CHROME_PROJECT.py")
	os.system("open /Applications/Google\ Chrome.app --args " + "-user-data-dir=" + TEMP_CHROME_PROFILE + " --load-extension=" + CHROME_EXTENSION_DIR + " \"" +  ccUrl + "\"")
elif _platform == "linux" or _platform == "linux2":
	os.system("./BUILD_CHROME_PROJECT.py")
	os.system("chromium " + "-user-data-dir=" + TEMP_CHROME_PROFILE + " --load-extension=" + CHROME_EXTENSION_DIR + " \"" +  ccUrl + "\"")
	shutil.rmtree(TEMP_DIR)
