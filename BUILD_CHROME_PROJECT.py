#!/usr/bin/env python2

from distutils.dir_util import copy_tree
import os
import shutil

CHROME_DIR = "Chrome-Extension"


if os.path.exists(CHROME_DIR):
	print "Extension directory already exists. Deleting contents..."
	shutil.rmtree(CHROME_DIR)

print "Creating extension directory..."
os.makedirs(CHROME_DIR)

print "Copying contents from sources..."

def CopyFiles(copyFrom, to):
    for name in os.listdir(copyFrom):
        if name.endswith(".js") or name.endswith(".json") or name.endswith(".png") or name.endswith(".css") or name.endswith(".html") or name.endswith(".gif"):
            pathName = os.path.join(copyFrom, name)
            if os.path.isfile(pathName):
                shutil.copy2(pathName, to)

def BuildTsFiles(dictory):
    for name in os.listdir(dictory):
        if name.endswith(".ts"):
            os.system("tsc " + directory + "/" + name)
            
def RemoveGeneratedJSFiles(directory):
    for name in os.listdir(directory):
        if name.endswith(".js"):
            os.remove(directory + "/" + name)
            
directories = ["Common", "Common/CSS", "Common/Images", "Common/Templates", "Common/Code", "Common/Code/Parsers", "Common/Code/UI", "Common/Code/Models", "Chrome", "3rdParty"]

for directory in directories:
    BuildTsFiles(directory)
    CopyFiles(directory, CHROME_DIR)
    
RemoveGeneratedJSFiles("Common")
RemoveGeneratedJSFiles("Common/CSS")
RemoveGeneratedJSFiles("Common/Images")
RemoveGeneratedJSFiles("Common/Templates")
RemoveGeneratedJSFiles("Common/Code")
RemoveGeneratedJSFiles("Common/Code/Models")
RemoveGeneratedJSFiles("Common/Code/Parsers")
RemoveGeneratedJSFiles("Common/Code/UI")
RemoveGeneratedJSFiles("Chrome")    

print "Chrome Extension has been packaged to 'Chrome-Extension'. Please point the Chrome extension to the directory!"
